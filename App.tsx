import 'react-native-gesture-handler'
import React from 'react'
import { Provider } from 'react-redux';
import store  from './src/store';
import { NavigationContainer } from '@react-navigation/native';
//import { StackNavigator } from './src/navigator/StackNavigator';
import { TabsNavigator } from './src/navigator/TabsNavigator';

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        {/* <StackNavigator /> */}
        <TabsNavigator />
      </NavigationContainer>
    </Provider>
  )
}
export default App;
