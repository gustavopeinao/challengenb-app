import { ACT_PRODUCTS_CHANGE, ACT_PRODUCT_CHANGE } from './typesActions';
import ProductsService from '../providers/productsService';

export function setLoadingProducts(loading){
	return {
		type: ACT_PRODUCTS_CHANGE,
		props: {
			loading,
		}
	};
}

export function setLoadingProduct(loading){
	return {
		type: ACT_PRODUCT_CHANGE,
		props: {
			loading,
		}
	};
}

export function setProducts(products) {
	return {
		type: ACT_PRODUCTS_CHANGE,
		props: {
			products,
			loading: false
		}
	};
}

export function setProduct(product) {
	return {
		type: ACT_PRODUCT_CHANGE,
		props: {
			product,
			loading: false
		}
	};
}

export function setErrorProducts(error){
	return {
		type: ACT_PRODUCTS_CHANGE,
		props: {
			response: error,
			loading: false,
		}
	};
}

export function setErrorProduct(error){
	return {
		type: ACT_PRODUCT_CHANGE,
		props: {
			response: error,
			loading: false,
		}
	};
}


export function actGetProducts() {
	console.log('actGetProducts')
	return async (dispatch) => {
		dispatch(setLoadingProducts(true));
		try {
			const productsResponse = await ProductsService.getProducts();
			// console.log('productsResponse',productsResponse.data.results)
			dispatch(setProducts(productsResponse.data.results));
		} catch (error) {
			dispatch(setErrorProducts(error));
		}
	};
}

export function actGetProductById(id) {
	console.log('actGetProductById',id)
	return async (dispatch) => {
		dispatch(setLoadingProduct(true));
		try {
			const productResponse = await ProductsService.getProductsById(id);
			console.log('productResponse',productResponse.data)
			dispatch(setProduct(productResponse.data));
		} catch (error) {
			dispatch(setErrorProduct(error));
		}
	};
}