import React, { useState, useRef } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Picker as RNPicker } from '@react-native-picker/picker';

interface Props {
    items: Array<{}>
    value: string
    setValue: (v: string) => {}
}
export const Picker = ({ items, value, setValue }: Props) => {
    return (
        <View style={styles.containerp}>
            <RNPicker
                dropdownIconColor='black'
                selectedValue={value}
                onValueChange={v =>
                    setValue(v)
                }
                style={styles.inputAndroid}
            >
                {items.map((item, key) => {
                    return (<RNPicker.Item key={key} label={item.label} value={item.value} />)
                })}
            </RNPicker>
        </View>
    )
}
const styles = StyleSheet.create({
    containerp: {
        color: '#000000',
        borderColor: '#8586874D',
        borderRadius: 8,
        borderWidth: 1,
        marginBottom: 5,
        justifyContent: 'center'
    },
    inputAndroid: {
        // padding: 10,
        fontSize: 16,
        color: 'gray',
    }
})
