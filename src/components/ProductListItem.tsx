import React, { useEffect } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, Dimensions } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import {formatMoney} from '../constants'

const {width: windowWidth} = Dimensions.get('window');

//import { ProductItem } from '../interfaces';

// interface Props {
//     productItem: ProductItem
// }

const ProductListItem = ({ productItem }: any) => {
  
  const navigator = useNavigation()
  // console.log('productItem',productItem)

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => navigator.navigate('ProductScreen', productItem.id)}
    >
      <View style={styles.target}>
        <Image
          source={{ uri: productItem.thumbnail }}
          style={styles.image}
        />
        <View style={styles.textContainer}>
          <Text style={styles.targetTitle}>{productItem.title}</Text>
          <Text style={styles.targetPrice}>{formatMoney(productItem.price, productItem.currency_id)}</Text>
          <View style={{flexDirection:'row', justifyContent:'space-between', paddingRight:50}}>
            <Text style={styles.targetPrice}>{productItem.sold_quantity} vendidos</Text>
            <Text style={styles.targetCondition}>{productItem.condition?'Nuevo':''}</Text>
          </View>
          <Text style={styles.targetPrice}>Disponibles {productItem.order_backend}</Text>
        </View>

        {/* <View style={{ flex: 1 }}></View> */}
        {/* <View style={styles.iconContainer}>
          <Icon
            name='chevron-forward-outline'
            color='gray'
            size={23}
          />
        </View> */}

      </View>
    </TouchableOpacity>
  )
}
export default ProductListItem;
const styles = StyleSheet.create({
  target: {
    // backgroundColor:'yellow',
    flexDirection: 'row'
  },
  image: {
    width:(windowWidth*0.25),
    height:(windowWidth*0.25),
    borderRadius: (windowWidth*0.25)/2
  },
  textContainer: {
    width:(windowWidth*0.75),
    // backgroundColor:'orange',
  },
  targetTitle: {
    //width:'100%',
    // maxWidth:(windowWidth*0.75),
    fontSize: 15,
    fontWeight: 'bold',
    color: 'black'
  },
  targetPrice: {
    fontSize: 15,
    color: 'gray'
  },
  targetCondition: {
    fontSize: 15,
    color: 'green',
    // justifyContent:'flex-end',
    // textAlign:'right'
  },
  
})
