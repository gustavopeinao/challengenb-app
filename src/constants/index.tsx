export const formatMoney = (number: number, currency: string) => {
    return '$' + number.toLocaleString('es-ar', { currency });
  }