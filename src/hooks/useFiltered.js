export const useFiltered = (products, price, condition) => {

    // (notifications, searchText, clasification, tipe)
    // if (searchText === '' && clasification === '' && tipe === '') { return notifications }

    // searchText = searchText.toLowerCase();

    // let notisFil = notifications.filter(n => n.template.titulo.toLowerCase().includes(searchText));
    // if (clasification !== '') notisFil = notisFil.filter(n => n.template.area.id===clasification);
    // if (tipe !== '') notisFil = notisFil.filter(n => n.tipo.toLowerCase().includes(tipe));
    // return notisFil;
    console.log('products', products)
    console.log('price', price)

    let prodPrice=[]
    //objs.sort((a,b) => (a.last_nom > b.last_nom) ? 1 : ((b.last_nom > a.last_nom) ? -1 : 0))
    if (price == 'asc') {
        prodPrice = products.sort((a, b) => (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0))
    } else {
        prodPrice = products.sort((a, b) => (a.price < b.price) ? 1 : ((b.price < a.price) ? -1 : 0))
    }
    let prodFiltered = prodPrice.filter(p=>p.condition==condition)
    return prodFiltered

}