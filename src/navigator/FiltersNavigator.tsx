import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { FiltersScreen } from "../screens/FiltersScreen";
import { ProductScreen } from "../screens/ProductScreen";
import ProductListScreen  from "../screens/ProductListScreen";

const Stack = createStackNavigator();

export const FiltersNavigator = () => {
    return (
        <Stack.Navigator
            initialRouteName="FiltersScreen"
            screenOptions={{
                headerShown: false
            }}
        >
            <Stack.Screen name="FiltersScreen" component={FiltersScreen} />
            <Stack.Screen name="ProductListScreen" component={ProductListScreen} />
            <Stack.Screen name="ProductScreen" component={ProductScreen} />
        </Stack.Navigator>
    );
}
