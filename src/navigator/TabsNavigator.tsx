import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeNavigator } from './HomeNavigator';
import { FiltersScreen } from '../screens/FiltersScreen';
import Icon from "react-native-vector-icons/Ionicons";


const Tab = createBottomTabNavigator();

export const TabsNavigator = () => {
    return (
        <Tab.Navigator
            sceneContainerStyle={{ backgroundColor: 'white' }}
            tabBarOptions={{
                activeTintColor: 'blue',
                labelStyle: {
                    marginBottom: 10,
                },
                style: {
                    elevation: 10,
                    height: 60
                }
            }}
        >
            <Tab.Screen
                name="StackNavigator"
                component={HomeNavigator}
                options={{
                    tabBarLabel: 'General',
                    tabBarIcon: ({ color }) => (
                        <Icon
                            color={color}
                            size={20}
                            name="home-outline"
                        />
                    )
                }}
            />
            <Tab.Screen
                name="FiltersScreen"
                component={FiltersScreen}
                options={{
                    tabBarLabel: 'Filtrar',
                    tabBarIcon: ({color}) => (
                        <Icon
                            color={color}
                            size={20}
                            name="list-outline"
                        />
                    )
                }}
            />
        </Tab.Navigator>
    );
}