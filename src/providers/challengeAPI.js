//import { default as axios } from 'axios';
import axios from "axios";

// const client = axios.create({
//     baseURL: process.env.REACT_APP_CHALLENGE_BASE_URL,
//     withCredentials: true, // Windows Authentication
//     timeout: 99999,
// });

export const getProducts = () => {
    return axios.get('https://api.mercadolibre.com/sites/MLA/search?q=iphone');
};

export const getProductsById = (id) => {
    return axios.get(`https://api.mercadolibre.com/items/${id}`);
};