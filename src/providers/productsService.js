import { getProducts, getProductsById } from './challengeAPI';

export default class ProductsService {
    static async getProducts() {
        return await getProducts();
    }
    static async getProductsById(id) {
        return await getProductsById(id);
    }
}