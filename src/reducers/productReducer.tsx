import { ACT_PRODUCT_CHANGE } from '../actions/typesActions';

const initialState = {
    product: null,
    response:null,
    error: false,
    loading: false,
};

const productReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case ACT_PRODUCT_CHANGE:
            return Object.assign({}, state, action.props);
        default:
            return state;
    }
}
export default productReducer;