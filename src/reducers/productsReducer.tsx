import { ACT_PRODUCTS_CHANGE } from '../actions/typesActions';

const initialState = {
    products: [],
    response:null,
    error: false,
    loading: false,
};

const productsReducer = (state = initialState, action : any) => {
    switch (action.type) {
        case ACT_PRODUCTS_CHANGE:
			return Object.assign({}, state, action.props);
		default:
			return state;
    }
}
export default productsReducer;