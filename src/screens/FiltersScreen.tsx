import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TextInput, Dimensions, Button, Alert, TouchableOpacity } from 'react-native'
import { Picker } from '../components/Picker'
import CurrencyInput from 'react-native-currency-input'
import { useSelector } from 'react-redux';
import ProductList from './ProductListScreen';
import { useFiltered } from '../hooks/useFiltered';
import Icon from "react-native-vector-icons/Ionicons";


const { width: windowWidth } = Dimensions.get('window');

export const FiltersScreen = () => {


  const prices: Array<{}> = [
    {
      label: 'Ascendente',
      value: 'asc'
    },
    {
      label: 'Descendente',
      value: 'des'
    }
  ]
  const conditions: Array<{}> = [
    {
      label: 'Nuevo',
      value: 'new'
    },
    {
      label: 'Usado',
      value: 'used'
    }
  ]
  const [price, setPrice] = useState('asc');
  const [condition, setCondition] = useState('new');
  const [initPrice, setInitPrice] = useState(null);
  const [endPrice, setEndPrice] = useState(null);

  const [productsFilterList, setProductsFilterList] = useState([]);

  useEffect(() => {
    console.log('PRICE-COND', price, condition)
  }, [price, condition])

  console.log('PRICE-COND', price, condition)
  //---hookFilter
  const { products, loading } = useSelector<any>(state => state.products);
  const [productsFiltered, setProductsFiltered] = useState([]);
  const [find, setFind] = useState(false);

  useEffect(() => {
    setFind(false)
  }, [condition, price])
  const findProducts = () => {
    setFind(true)
    console.log('findProducts', findProducts)
    let prodsFiltered = useFiltered(products, price, condition)
    setProductsFiltered(prodsFiltered)
  }

  console.log('productsFiltered', productsFiltered)
  const resetFilters = () => {
    setFind(false)
    setProductsFiltered([])
  }
  return (
    <>
      {
        productsFiltered.length === 0 ?
          <View style={styles.container}>
            <Text style={styles.label}>Orden (precio)</Text>
            <Picker
              items={prices}
              value={price}
              setValue={setPrice}
            />
            <Text style={styles.label}>Condición</Text>
            <Picker
              items={conditions}
              value={condition}
              setValue={setCondition}
            />
            <Text style={styles.label}>Rango de precio</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View>
                <Text style={styles.labelText}>Desde</Text>
                <CurrencyInput
                  value={initPrice}
                  onChangeValue={setInitPrice}
                  prefix="$"
                  delimiter="."
                  precision={2}
                  // onChangeText={(formattedValue) => {
                  //   console.log(formattedValue); // $2,310.46
                  // }}
                  style={styles.input}
                />
              </View>

              <View>
                <Text style={styles.labelText}>Hasta</Text>
                <CurrencyInput
                  value={endPrice}
                  onChangeValue={setEndPrice}
                  prefix="$"
                  delimiter="."
                  precision={2}
                  // onChangeText={(formattedValue) => {
                  //   console.log(formattedValue); // $2,310.46
                  // }}
                  style={styles.input}
                />
              </View>
            </View>
            {
              find &&
              <View style={{flex:1, justifyContent:'center'}}>
                <Text style={styles.labelNotFind}>No se encontraron productos :(</Text>
              </View>

            }
            <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 30 }}>
              <Button
                title="Buscar"
                // onPress={() => navigator.navigate('ProductListScreen', productsFilterList)}
                onPress={findProducts}
              />
            </View>
          </View> :
          <>
            <TouchableOpacity style={styles.back}
              onPress={resetFilters}>
              <Icon
                name='arrow-back-outline'
                color='blue'
                size={23}
              />
              <Text style={styles.labelBack}>Volver a filtrar</Text>
            </TouchableOpacity>
            <ProductList
              products={productsFiltered}
            />
          </>
      }
    </>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 20,
    marginTop: 20
  },
  label: {
    color: 'black',
    fontSize: 20
  },
  labelNotFind: {
    color: 'red',
    fontSize: 20,
    textAlign: 'center'
  },
  labelText: {
    paddingLeft: 5,
    fontSize: 16,
    color: 'gray',
  },
  input: {
    //color: '#000000',
    borderColor: '#8586874D',
    borderRadius: 8,
    borderWidth: 1,
    marginBottom: 5,
    justifyContent: 'center',
    fontSize: 16,
    color: 'gray',
    height: 55,
    width: (windowWidth / 2) * 0.8
  },
  labelBack: {
    color: 'blue',
    fontSize: 20
  },
  back: {
    flexDirection: 'row',
    marginVertical: 15,
    alignItems: 'center',
    marginLeft: 20
  }
})
