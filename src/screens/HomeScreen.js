import React, { useEffect } from 'react'
import { View, FlatList, StyleSheet, ActivityIndicator, Dimensions } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';
import { actGetProducts } from '../actions/productsActions';
import ProductList from './ProductListScreen';

export const HomeScreen = () => {
    const dispatch = useDispatch();
    const { products, loading } = useSelector(state => state.products);

    useEffect(() => {
        dispatch(actGetProducts())
    }, [])

    return (
        <>
            {loading ?
                <View style={styles.indicator}>
                    <ActivityIndicator size={40} color='blue' />
                </View>
                :
                <ProductList
                    products={products}
                />
            }
        </>
    )
}

const styles = StyleSheet.create({
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
