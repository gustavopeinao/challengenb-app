import React from 'react'
import { View, Text, FlatList, StyleSheet } from 'react-native'
import ProductListItem from '../components/ProductListItem'


const itemseparator = () => {
    return (
        <View style={styles.separator} />
    )
}

const ProductListScreen = ({ products }: any) => {
    return (
        <>
            {
                products.length == 0 ?
                    <View style={styles.textContainer}>
                        <Text style={styles.text}> :( No se encontraron productos</Text>
                    </View> :
                    <View style={styles.container}>
                        <FlatList
                            data={products}
                            renderItem={({ item }) => <ProductListItem productItem={item} />}
                            keyExtractor={(item) => item.id}
                            ItemSeparatorComponent={itemseparator}
                        />
                    </View>


            }
        </>

    )
}
export default ProductListScreen;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
    },
    textContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {

        fontSize: 20,
        color: 'gray'
    },
    separator: {
        borderBottomWidth: 1,
        borderColor: 'gray',
        opacity: 0.4,
        marginVertical: 8
    }
})
