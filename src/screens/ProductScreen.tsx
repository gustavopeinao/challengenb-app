import React, { useEffect } from 'react'
import { View, Text, Button, Image, StyleSheet, Dimensions, ScrollView, ActivityIndicator, TouchableOpacity } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';
import { actGetProductById } from '../actions/productsActions';
import { StackScreenProps } from '@react-navigation/stack';
import Carousel from 'react-native-snap-carousel';
import { formatMoney } from '../constants';
import Icon from "react-native-vector-icons/Ionicons";



const screenHeght = Dimensions.get('screen').height;
const screenWidth = Dimensions.get('window').width;

interface Props extends StackScreenProps<any, any> { };

export const ProductScreen = ({ route, navigation }: Props) => {
    const dispatch: any = useDispatch();
    const { product, loading }: any = useSelector<any>(state => state.product);
    const id = route.params as any

    useEffect(() => {
        console.log('id', id)
        dispatch(actGetProductById(id))
    }, [])

    console.log('product', product)
    console.log('loading', loading)
    return (
        <ScrollView style={styles.imageContainer}>
            {(loading||!product) ?
                <View style={{ height: screenHeght * 0.5, justifyContent: 'center', alignContent: "center" }}>
                    <ActivityIndicator color="blue" size={50} />
                </View> :
                
                <>
                    <Carousel
                        data={product && product.pictures}
                        renderItem={({ item }: any) => <Image source={{ uri: item.url }} style={styles.image} />}
                        sliderWidth={screenWidth}
                        itemWidth={400}
                    />
                    <View style={styles.textContainer}>
                        <Text style={styles.title}>{product.title}</Text>
                        <Text style={styles.price}>{formatMoney(product.price, product.currency_id)}</Text>
                    </View>
                    <TouchableOpacity
                        style={{
                            backgroundColor: 'gray',
                            width: 40,
                            height: 40,
                            borderRadius: 20,
                            position: 'absolute',
                            top: 10,
                            left: 10
                        }}
                        onPress={() => navigation.pop()}>
                        <View style={{
                            top:3,
                            display: "flex",
                            alignSelf:"center",
                            alignItems:"center"
                        }}>
                            <Icon
                                color={'white'}
                                size={30}
                                name="arrow-back-outline"
                            />
                        </View>
                    </TouchableOpacity>

                </>
            }

        </ScrollView>
    )
}
const styles = StyleSheet.create({
    imageContainer: {
        width: '100%',
        height: screenHeght * 0.5,
        //backgroundColor: 'red'
    },
    textContainer: {
        marginHorizontal: 10
    },
    title: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 30
    },
    price: {
        color: 'black',
        fontSize: 30
    },
    image: {
        width: screenWidth,
        height: 500,
        resizeMode: "contain"
        //aspectRatio: 1,
    },
    circleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    circle: {
        width: 26,
        height: 26,
        borderRadius: 13,
        borderWidth: 1,
        borderColor: 'red'
    }
})